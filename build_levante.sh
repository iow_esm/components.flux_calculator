#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

#########################################################
#         Setup the environment                         #
#########################################################
# We need:                                              #
#
# - an Intel Fortran compiler
# - a compatible NetCDF library
# - a compiler supporting MPI (e.g. Intel MPI or OpenMPI)
# - a python3 environment
#
#########################################################

module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
module load intel-oneapi-mpi/2021.5.0-intel-2021.5.0
module load netcdf-fortran/4.5.3-intel-oneapi-mpi-2021.5.0-intel-2021.5.0
module load netcdf-c/4.8.1-intel-oneapi-mpi-2021.5.0-intel-2021.5.0
module load python3/2023.01-gcc-11.2.0

# define compilers for MPI wrappers
export I_MPI_CC=icc                         # cmake and MPI build flags
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort

FC=mpiifort
AR=ar
IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# include paths
export IOW_ESM_NETCDFF_INCLUDE="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/include"
export IOW_ESM_NETCDFF_LIBRARY="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/lib"
export IOW_ESM_NETCDFC_LIBRARY="/sw/spack-levante/netcdf-c-4.8.1-7dq6g2/lib"

# path to the python libs and the specific name
# PYTHON_LIB_PATH="/sw/tools/python/anaconda3/2019.03/skl/lib"
PYTHON_LIB_PATH="/sw/spack-levante/python-3.10.10-p24a46/lib"
PYTHON_LIB="python3.10"

#########################################################
#              Setup the environment done.              #
#  The rest should be changed only if really necessary! #
#########################################################

if [ $debug == "debug" ]; then
	FFLAGS="-O0 -r8 -fp-model precise -xHost -DUSE_DOUBLE_PRECISION -g -traceback -check all -DIOW_ESM_DEBUG"
	configuration="DEBUG"
else
	FFLAGS="-O3 -r8 -no-prec-div -fp-model fast=2 -xHost -DUSE_DOUBLE_PRECISION"
	configuration="PRODUCTION"
fi

OASIS3_LIB="${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/IOW_ESM_${configuration}"
build_dir="build_${configuration}"
bin_dir="bin_${configuration}"

INCLUDES="-I ${OASIS3_LIB}/build/lib/mct \
          -I${OASIS3_LIB}/build/lib/psmile.MPI1 \
          -I ${OASIS3_LIB}/build/lib/psmile.MPI1 \
          -I ${OASIS3_LIB}/build/lib/mct \
          ${OASIS3_LIB}/lib/libpsmile.MPI1.a \
          ${OASIS3_LIB}/lib/libmct.a \
          ${OASIS3_LIB}/lib/libmpeu.a \
          ${OASIS3_LIB}/lib/libscrip.a \
          -I${IOW_ESM_NETCDFF_INCLUDE}"
LIBS="-lnetcdf -lnetcdff -L${IOW_ESM_NETCDFC_LIBRARY} -L${IOW_ESM_NETCDFF_LIBRARY}"

if [ "$rebuild" == "rebuild" ]; then
	rm -r "${build_dir}"
	rm -r "${bin_dir}"
fi
mkdir -p ./"${build_dir}"
mkdir -p ./"${bin_dir}"

cd "${build_dir}"

$FC -c $FFLAGS ../src/flux_lib/constants/*.F90
$FC -c $FFLAGS ../src/flux_lib/auxiliaries/*.F90
$FC -c $FFLAGS ../src/flux_lib/mass/*.F90
$FC -c $FFLAGS ../src/flux_lib/heat/*.F90
$FC -c $FFLAGS ../src/flux_lib/momentum/*.F90
$FC -c $FFLAGS ../src/flux_lib/radiation/*.F90
$FC -c $FFLAGS ../src/flux_lib/*.F90
rm flux_library.a
$AR rv flux_library.a *.o

cp -r ${PWD}/../src/pyfort/* .
python3 pyfort_src.py "$PWD"
$FC -c -Wl,-rpath,"${PWD}" -L"${PWD}" -lpyfort call_python.f90

$FC -c $FFLAGS ../src/bias_corrections.F90 -I${IOW_ESM_NETCDFF_INCLUDE} $LIBS
$FC -c $FFLAGS ../src/flux_calculator_basic.F90 
$FC -c $FFLAGS ../src/flux_calculator_prepare.F90
$FC -c $FFLAGS ../src/flux_calculator_calculate.F90
$FC -c $FFLAGS ../src/flux_calculator_parse_arg.F90
$FC -c $FFLAGS ../src/flux_calculator_io.F90 -I${IOW_ESM_NETCDFF_INCLUDE} $LIBS
$FC -c $FFLAGS ../src/flux_calculator_create_namcouple.F90

$FC $FFLAGS -o ../"${bin_dir}"/flux_calculator ../src/flux_calculator.F90 flux_calculator_basic.o flux_calculator_prepare.o flux_calculator_calculate.o flux_calculator_io.o flux_calculator_parse_arg.o flux_calculator_create_namcouple.o bias_corrections.o call_python.o flux_library.a \
$INCLUDES  $LIBS -Wl,-rpath,"$PWD",-rpath,${IOW_ESM_NETCDFC_LIBRARY},-rpath,${IOW_ESM_NETCDFF_LIBRARY},-rpath="${PYTHON_LIB_PATH}" -L"${PYTHON_LIB_PATH}" -l"${PYTHON_LIB}" -L"$PWD" -lpyfort   

cd -
